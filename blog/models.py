from django.db import models

# Create your models here.
class Blog(models.Model):
    judul = models.CharField(max_length=50)
    deskripsi = models.TextField()
    tanggal = models.DateField

    def __str__(self):
        return self.judul
