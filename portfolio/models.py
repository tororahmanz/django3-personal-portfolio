from django.db import models

# Create your models here.
class Project(models.Model):
    judul = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=200)
    gambar = models.ImageField(upload_to='portfolio/images/')
    url = models.URLField(blank=True)

    def __str__(self):
        return self.judul
